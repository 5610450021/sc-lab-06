package item2;

public class Eating_Bear {
	public String makeBearEat(Panda panda){
		return "Panda: "+panda.eat();
	}
	public String makeBearEat(Polar_Bear polar){
		return "Palar: "+polar.eat();
	}
	public String makeBearEat(Grizzly_Bear grizzy){
		return "Grizzy: "+grizzy.eat();
	}
	public String makeBearEat(Bear bear){
		return "Bear: "+bear.eat();
	}
}
