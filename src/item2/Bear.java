package item2;

public abstract class Bear {
	private String name;
	
	public Bear(String name){
		this.name = name;
	}
	
	public abstract String eat();
	
	public String toString(){
		return this.name;
	}
}
