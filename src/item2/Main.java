package item2;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grizzly_Bear grizzy = new Grizzly_Bear("May");
		Panda  panda = new Panda("Pim");
		Polar_Bear polar = new Polar_Bear("Phuak");
		
		Eating_Bear eating = new Eating_Bear();
		System.out.println(eating.makeBearEat(grizzy));
		System.out.println(eating.makeBearEat(panda));
		System.out.println(eating.makeBearEat(polar));
		System.out.println(eating.makeBearEat((Bear) polar));
	}

}
